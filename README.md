# AED-Lab1

# Lab1-UI

Actividades correspondientes al Laboratorio 1 de Algortimo y Estructura de Datos.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta del ejercicio correspondiente (por ejemplo, "ej1") y ejecutar por terminal:
```
g++ programa.cpp Clase_correspondiente.cpp -o programa
make
./programa
```

# Actividades
- ej1
Escribir un programa que llene un arreglo unidimensional de números enteros y luego obtenga como resultado la suma del cuadrado de los números ingresados. (Considere un arreglo unidimensional de tipo entero de N elementos).
- ej2
Escribir un programa que lea N frases en un arreglo de caracteres y determine el número de minúsculas y mayúsculas que hay en cada una de ellas. Puede utilizar las funciones islower() y isupper() de la librerı́a ctype.h
- ej3
Una empresa registra para cada uno de sus N clientes los siguientes datos:
	- Nombre (cadena de caracteres)
	- Teléfono (cadena de caracteres)
	- Saldo (entero)
	- Moroso (booleano)
Escribir un programa que permita el ingreso de los datos de los clientes y luego permita imprimir sus datos indicando su estado (morosos o no). Utilice arreglo de clases en su solución.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
