/* 
 * g++ programa.cpp Numero.cpp -o programa
 */
#include <iostream>
#include "Numero.h"

Numero::Numero(int max){

	/* ejemplo profe */
	this->max = max;
	this->vector = new int[this->max]; //CREA ESPACIO DE MEMORIA

}

void Numero::operacion_SumaCuadrado(int *vector, int max){
	/* variable a la que se le suman el cuadrado de los numeros */
	int resultado=0;
	for (int i = 0; i < max; ++i)
	{
		/* elevar los numeros al cuadrado 
		 * y los suma */
		resultado += vector[i] * vector[i];
	}
	/* total */
	this->resultado=resultado;
}

int Numero::get_resultado(){
	return this->resultado;
}