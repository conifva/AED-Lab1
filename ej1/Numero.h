#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
	private:
		int max;
		int *vector = NULL;
		int resultado;

	public:
		/* constructores */
		Numero(int max);

		/* métodos */
		void operacion_SumaCuadrado(int *vector, int max);
		int get_resultado();

};
#endif
