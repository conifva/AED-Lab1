/*
 * g++ programa.cpp Numero.cpp -o programa
 */
#include <iostream>
#include "Numero.h"
using namespace std;

int main()
{
	/* intro */
	cout << "\n\t\t> BIENVENIDO" << endl;
	cout << "Este programa llena un arreglo unidimensional de números enteros" << endl;
	cout << "y luego calcula la suma del cuadrado de los números ingresados." << endl;

	int max;
	cout << "\n> Ingrese la cantidad de números que ingresará: ";
	cin >> max;
	
	int vector[max];
	
	for (int i = 0; i < max; ++i)
	{
		/* llenar arreglo */
		cout << "Ingrese el número de la posición <" << i << ">: ";
		cin >> vector[i];
	}

	Numero n = Numero(max);
	n.operacion_SumaCuadrado(vector, max);
	
	cout << "\n> El resultado es: " << n.get_resultado() << endl;
	
	return 0;
}
