#include <iostream>
#include "Contador.h"
#include <ctype.h>

using namespace std;

Contador::Contador(){
}

int Contador::get_lower(string frase){
	this->lower = 0; // contador en 0 (atributo lower de la class)
	/* a traves del largo del arreglo */
	for (int i = 0; i < frase.size(); i++)
	{
		/* contar minusculas */
		if (islower(frase[i]))
		{
			/* si encuentra suma 1 */
			this->lower++;
		}
	}
	/* devuelve la cantidad encontrada */
	return this->lower;
}

/* mismo sistema para las mayusculas */ 
int Contador::get_upper(string frase){
	this->upper = 0;
	for (int i = 0; i < frase.size(); i++)
	{
		if (isupper(frase[i]))
		{
			this->upper++;
		}
	}
	/* devuelve la cantidad encontrada */
	return this->upper;
}