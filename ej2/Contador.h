#include <iostream>
using namespace std;

#ifndef CONTADOR_H
#define CONTADOR_H

class Contador 
{
	private:
		int lower;
		int upper;

	public:
		/* constructor */
		Contador();

		/* getters*/
		int get_lower(string frase);
		int get_upper(string frase);
	
};
#endif