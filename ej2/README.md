# AEDLab1

# Ejercicio2 Lab1-UI

Programa que programa que lee frases determina el número de minúsculas
y mayúsculas que hay en cada una de ellas.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta "ej2" y ejecutar por terminal:
```
g++ programa.cpp Contador.cpp -o programa
make
./programa
```

# Acerca de
El inicio del programa se solicita ingresar la cantidad de frases las que se trabajará.
Luego, se requiere el ingreso de las frases una a una para finalmente mostrar el resultado por cada frase.

# Errores
- No estan considerados los errores por parte del usuario al momento de ingresar los datos. en caso de que ocurra esta equivocación el programa se detendrá.
- El arreglo no considera el texto ingresado como frases para contarlas, sino como palabras.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela