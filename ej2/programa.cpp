/*
 * g++ programa.cpp Contador.cpp -o programa
 */
#include <iostream>
#include "Contador.h"
using namespace std;

void salida(string *frase, int limite){
	/* crea objeto c para leer letras */
	Contador c = Contador();

	/* recorre la frase y mostar cintador */
	for(int i = 0; i < limite; i++){
		/* mostrar contador para cada frase */
		cout << "\nLa frase [" << i << "]:" << endl;
		cout << "- " << frase[i] << endl;
		cout << "\t+ Tiene " << c.get_lower(frase[i]) << " minusculas" << endl;
		cout << "\t+ Tiene " << c.get_upper(frase[i]) << " mayusculas" << endl;
		cout << "-----------------------------------------" << endl;
	}
}

int main()
{
	/* INTRO */
	cout << "\n\t\t\tBIENVENIDO" << endl;
	cout << "Este programa que lee N frases en un arreglo de caracteres y determina" << endl;
	cout << "el número de minúsculas y mayúsculas que hay en cada una de ellas." << endl;
	cout << "\n\n\t\t>> CONTADOR DE MAYUSCULAS/MINUSCULAS <<" << endl;
	
	int limite;
	cout << "\n> Ingrese la cantidad de frases: ";
	cin >> limite;

	/* arreglo de cadenas de caracteres */
	string frase[limite];
	
	/* agrega frases al arreglo */
	for (int i = 0; i < limite; i++){
		/* llenar cada frase */
		cout << "Ingrese la frase [" << i << "]: ";
		cin >> frase[i];
	}

	/* mostrar resumen (resultado) */
	salida(frase, limite);

	return 0;
}