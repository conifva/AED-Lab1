# AEDLab1

# Ejercicio3 Lab1-UI

Programa que permite el ingreso de los datos de los clientes de una empresa y luego imprimirlos indicando su estado de morosidad.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta "ej3" y ejecutar por terminal:
```
g++ programa.cpp Cliente.cpp -o programa
make
./programa
```

# Acerca de
El inicio del programa se solicita la cantidad de clientes a ingresar.
A continuación, se solicita completar los datos de cada cliente e indicar si estos son morosos o no.
Finalmente se muestran por terminal los datos correspondientes a cada cliente.

# Errores
- Solo se permite el ingreso de una palabra para cuando se solicita el Nombre del cliente, si se ingresa más de una el programa se detiene y se autocompletan los demas datos.
- El programa se detiene si se ingresan caracteres en las categorias donde se solicitan numeros.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela