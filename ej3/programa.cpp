#include <iostream>
#include <stdio.h>
#include "Cliente.h"

using namespace std;

void imprimir_datos(int cant_clientes, Cliente *cliente){
	/* recorre arreglo de clientes y obtiene los datos de c/u */
	for (int i = 0; i < cant_clientes; i++){
		/* Mostrar datos*/
		cout << "\n\n\tDATOS CLIENTE" << endl;
		cout << "  + Nombre: " << cliente[i].get_nombre() << endl;
		cout << "  + Teléfono: " << cliente[i].get_telefono() << endl;
		cout << "  + Saldo de la deuda: " << cliente[i].get_saldo() << endl;
	
		/* Morosidad */
		if(cliente[i].get_estado()){
			cout << "    CLIENTE MOROSO" << endl;
		}
		else{
			cout << "    CLIENTE SIN DEUDA" << endl;
		}
		cout << "--------------------------------------" << endl;
	}
}
	
void llenar_datos(int cant_clientes, Cliente *cliente){
	string nombre;
	int telefono;
	int saldo;
	string estado;

	for (int i = 0; i < cant_clientes; i++){
		/* ingreso de datos */
		cout << "\n\tCLIENTE [" << i << "]" << endl;
		cout << "Nombre: ";
		cin >> nombre;
		cout << "Telefono: ";
		cin >> telefono;
		cout << "Saldo: $ ";
		cin >> saldo;
		cout << "¿El cliente es moroso? (s/n): ";
		cin >> estado;

		while(estado != "s" && estado != "n"){
			cout << "Opcion invalida. Intentelo nuevamente utilizando s/n";
			cin >> estado;
		}
		/* agrega datos */
		cliente[i].set_nombre(nombre);
		cliente[i].set_telefono(telefono);
		cliente[i].set_saldo(saldo);

		if(estado == "s"){
			cliente[i].set_estado(1);
		}
		else{
			cliente[i].set_estado(0);
		}
	}
}

int main(){
	int cant_clientes;

	cout << "\n\tBIENVENIDO";
	cout << "\n>Ingrese la cantidad de clientes: ";
	cin >> cant_clientes;

	/* arreglo de tipo Cliente */
	Cliente cliente[cant_clientes];
	/* pasos: 
	 *  - llenar datos arreglo
	 *  - mostrar por terminal
	 */
	llenar_datos(cant_clientes, cliente);
	imprimir_datos(cant_clientes, cliente);

	return 0;

}
